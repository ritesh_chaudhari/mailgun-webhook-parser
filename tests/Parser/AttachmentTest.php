<?php declare (strict_types = 1);

namespace MailgunParser\Parser;

use MailgunParser\Parser;
use PHPUnit\Framework\TestCase;

class AttachmentTest extends TestCase
{
    /** @var Attachment */
    private $attachment;

    /** @var mixed */
    private $data;

    /** @var mixed */
    private $apiKey;

    /** @var mixed */
    private $adapter;

    protected function setUp()
    {
        $this->data = null;
        $this->apiKey = null;
        $this->adapter = null;
        $this->attachment = new Attachment(
            $this->data,
            $this->apiKey,
            $this->adapter
        );
    }
    public function testGetAll()
    {
        $this->assertEquals($this->attachment->getAll(), null);
    }
    public function testStoreAll()
    {
        $this->assertEquals([], $this->attachment->storeAll('/'));
    }
}

<?php
declare (strict_types = 1);
/*
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */
namespace MailgunParser;

/**
 * This class is the base class for the Mailgun Mail Parser.
 */
class MailgunParser
{
    /**
     * @var string|null
     */
    private $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function parse($data = null): Parser\DataParser
    {
        $parser = new Parser\DataParser($this->apiKey);
        return $parser->parse($data);
    }
}

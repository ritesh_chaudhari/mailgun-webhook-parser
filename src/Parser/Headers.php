<?php

declare (strict_types = 1);

/*
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace MailgunParser\Parser;

use GuzzleHttp\Client;
use Webmozart\Assert\Assert;

/**
 * @author Ritesh Chaudhari <itsyub@gmail.com>
 */
class Headers
{
    private $client = null;
    private $apiKey = null;
    private $parsedData = [];
    public function __construct($data, $apiKey)
    {
        $this->client = new Client();
        $this->apiKey = $apiKey;
        $this->parsedData = $data;
    }
    public function getAll()
    {
        $headers = json_decode($this->parsedData['message-headers'] ?? '', true);
        if (!empty($headers)) {
            foreach ($headers as $key => $header) {
                $data[$header[0]]=$header[1];
            }
            return $data;
        } else {
            return [];
        }
    }    
}

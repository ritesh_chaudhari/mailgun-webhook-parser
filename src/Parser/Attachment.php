<?php

declare (strict_types = 1);

/*
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace MailgunParser\Parser;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use League\Flysystem\Filesystem;
use Webmozart\Assert\Assert;

/**
 * @author Ritesh Chaudhari <itsyub@gmail.com>
 */
class Attachment
{
    private $client = null;
    private $apiKey = null;
    private $attachments = [];
    private $attachmentData = null;
    private $message_id = '';
    private $adapter = null;
    private $whiteListMimes = ['image/png', 'image/jpeg', 'image/jpg', 'application/pdf'];
    public function __construct($data, $apiKey, $adapter, $whiteListMimes = [])
    {
        $this->client = new Client();
        $this->apiKey = $apiKey;
        $this->attachments = json_decode($data['attachments'] ?? '', true);
        $this->message_id = preg_replace('/[<>]/', '', $data['Message-Id'] ?? '');
        $this->adapter = $adapter;
        if (!empty($whiteListMimes)) {
            $this->whiteListMimes = $whiteListMimes;
        }
    }

    public function getAll()
    {
        return $this->attachments;
    }

    public function storeAll(string $folder_path)
    {
        Assert::stringNotEmpty($folder_path);
        $attachments = $this->attachments;
        $data = [];
        if (!empty($attachments)) {
            foreach ($attachments as $key => $attachment) {
                $result = $this->request($attachment['url'], $this->apiKey);
                if ($result !== false) {
                    $result = $this->store($folder_path, sha1($this->message_id . '_' . $attachment['name']), $attachment['content-type']);
                    if ($result !== false) {
                        $attachment['file_path'] = $result;
                        $data[] = $attachment;
                    } else {
                        $data[$key] = false;
                    }
                } else {
                    $data[$key] = false;
                }
            }
            return $data;
        } else {
            return [];
        }
    }
    public function request(string $url)
    {
        Assert::stringNotEmpty($url);
        Assert::regex($url, '@https://.*mailgun\.(net|org)/v.+@');
        Assert::regex($url, '|/attachments/[0-9]+|');
        try {
            $response = $this->client->get($url, [
                'auth' => ['api', $this->apiKey],
            ]);
        } catch (RequestException $e) {
            if (404 === $e->getResponse()->getStatusCode()) {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
        if (200 !== $response->getStatusCode()) {
            return $this;
        } else {
            $this->attachmentData = (string) $response->getBody();
            return $this;
        }
    }

    public function getContent()
    {
        return $this->attachmentData;
    }

    public function store(string $path, string $filename, string $content_type)
    {
        Assert::stringNotEmpty($this->attachmentData);
        Assert::directory($path);
        Assert::writable($path);
        if (in_array($content_type, $this->whiteListMimes)) {
            if ($this->adapter !== null) {
                $filesystem = new Filesystem($this->adapter);
                $response = $filesystem->write($filename, $this->attachmentData);
                return $response === false ? false : $path . $filename;
            } else {
                return file_put_contents($path . $filename, $this->attachmentData) === false ? false : $path . $filename;
            }
        } else {
            return false;
        }
    }
}

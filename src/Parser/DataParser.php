<?php

declare (strict_types = 1);

/*
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace MailgunParser\Parser;

use GuzzleHttp\Client;
use MailgunParser\Parser;

/**
 * @author Ritesh Chaudhari <itsyub@gmail.com>
 */
class DataParser
{
    private $client = null;
    private $apiKey = null;
    private $parsedData = null;
    private $rawJSONdata = null;

    public function __construct($apiKey)
    {
        $this->client = new Client();
        $this->apiKey = $apiKey;
    }

    public function parse($data = null): Parser\DataParser
    {
        if ($data === null) {
            $data = $_POST;
        }
        if (!is_array($data)) {
            $parsedData = json_decode(str_replace('\\\\', '\\\\\\\\', $data), true);
        } else {
            $parsedData = $data;
            $this->rawJSONdata = json_encode($data);
        }
        if ($parsedData !== null && is_array($parsedData) && !empty($parsedData)) {
            $this->parsedData = $parsedData;
            return $this;
        } else {
            throw new \Exception('Invalid JSON data');
        }
    }

    public function verify(): bool
    {
        $token = $this->parsedData['token'] ?? '';
        $timestamp = $this->parsedData['timestamp'] ?? '';
        $signature = $this->parsedData['signature'] ?? '';

        if ($token !== '' && $timestamp !== '' && $signature !== '') {
            // check if the timestamp is fresh
            if (abs(time() - $timestamp) > 10000) {
                return false;
            }
            // returns true if signature is valid
            return hash_hmac('sha256', $timestamp . $token, $this->apiKey) === $signature;
        } else {
            return false;
        }
    }

    public function attachments($adapter = null): Parser\Attachment
    {
        $attachments = new Parser\Attachment($this->parsedData, $this->apiKey, $adapter);
        return $attachments;
    }

    public function headers(): Parser\Headers
    {
        $headers = new Parser\Headers($this->parsedData, $this->apiKey);
        return $headers;
    }

    public function body(): Parser\Body
    {
        $body = new Parser\Body($this->parsedData, $this->apiKey);
        return $body;
    }

    public function isSpam(): bool
    {
        if (($this->parsedData['X-Mailgun-Sflag'] ?? 'No') === 'Yes') {
            return true;
        } else {
            return false;
        }
    }

    public function getContent()
    {
        return $this->parsedData;
    }

    public function saveToFile()
    {
        $message_id = $this->body()->getMessageId();
        $result = file_put_contents(getenv('STORE_PATH') . 'messages/' . $message_id . '.json', $this->rawJSONdata);
    }
}

<?php

declare (strict_types = 1);

/*
 *
 * This software may be modified and distributed under the terms
 * of the MIT license. See the LICENSE file for details.
 */

namespace MailgunParser\Parser;

use GuzzleHttp\Client;

/**
 * @author Ritesh Chaudhari <itsyub@gmail.com>
 */
class Body
{
    private $client = null;
    private $apiKey = null;
    private $parsedData = [];

    public function __construct($data, $apiKey)
    {
        $this->client = new Client();
        $this->apiKey = $apiKey;
        $this->parsedData = $data;
    }

    public function getMessageId()
    {
        return preg_replace('/[<>]/', '', $this->parsedData['Message-Id'] ?? '');
    }

    public function getBodyRaw()
    {
        return $this->parsedData['body-html'] ?? '';
    }

    public function getBodyStripped()
    {
        return $this->parsedData['stripped-html'] ?? '';
    }

    public function getBodyPlain()
    {
        return $this->parsedData['body-plain'] ?? '';
    }

    public function getSubject()
    {
        return $this->parsedData['Subject'] ?? '';
    }

    public function getSender()
    {
        return $this->parsedData['sender'] ?? '';
    }

    public function getRecipientEmail()
    {
        return $this->parsedData['recipient'] ?? '';
    }

    public function getRecipientName()
    {
        return preg_replace('/\ <.*?>/', '', $this->parsedData['from'] ?? '');
    }

    public function isReply()
    {
        return isset($this->parsedData['In-Reply-To']) ? true : false;
    }

    public function getRepliedToEmail()
    {
        return preg_replace('/[<>]/', '', $this->parsedData['In-Reply-To'] ?? '');
    }

    public function getCC()
    {
        $cc_string = $this->parsedData['Cc'] ?? '';
        $cc_array = explode(', ', $cc_string);
        foreach ($cc_array as $key => $cc_recipient) {
            $cc_recipient = preg_match('/([a-zA-Z0-9\s]+) <([a-zA-Z0-9\s@.]+)>/', $cc_recipient, $matches);
            if (count($matches) == 3) {
                array_shift($matches);
                $cc_array[$key] = [];
                $cc_array[$key]['name'] = $matches[0] ?? '';
                $cc_array[$key]['email'] = $matches[1] ?? '';
            } else {
                $cc_array[$key] = [];
            }
        }
        return $cc_array;
    }

    public function getDomain()
    {
        return $this->parsedData['domain'] ?? '';
    }

    public function getMailerClient()
    {
        return preg_replace('/\ \([0-9\.]+\)/', '', $this->parsedData['X-Mailer'] ?? '');
    }
}

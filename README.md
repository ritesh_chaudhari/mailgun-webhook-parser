# mailgun parser to use as webhook handler

## Example

``` php
<?php
/**
 * This is example file for how to use mailparser
 */
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set("error_log", "/tmp/php-error.log");

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
    echo "only post method allowed";
    return;
}

require 'vendor/autoload.php';

use MailgunParser\MailgunParser;

use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;

putenv('MAILGUN_APP_KEY=key-xxxxxxxxxxxxxx');
putenv('STORE_PATH=/var/uploads/');

$mg = new MailgunParser(getenv('MAILGUN_APP_KEY'));

try {
    $parsedData = $mg->parse();
} catch (Exception $e) {
    echo $e->getMessage();
    return;
}
//to verify posted data comes from mailgun
$verified = $parsedData->verify();

if ($verified) {

    $adapter = new Local(getenv('STORE_PATH').'attachments/');

    $parsedData->saveToFile();
    var_dump($parsedData->attachments()->getAll());
    var_dump($parsedData->attachments($adapter)->storeAll(getenv('STORE_PATH') . 'attachments/'));
    var_dump($parsedData->getContent());
    var_dump($parsedData->headers()->getAll());
    $body = $parsedData->body();
    echo "is Spam: " . $parsedData->isSpam();
    var_dump($body->getCC());
    var_dump($body->getBodyRaw());
    var_dump($body->getBodyStripped());
    var_dump($body->getBodyPlain());
    var_dump('subject', $body->getSubject());
    var_dump('sender', $body->getSender());
    var_dump('recipient email', $body->getRecipientEmail());
    var_dump('recipient name', $body->getRecipientName());
    var_dump('mail client', $body->getMailerClient());
    var_dump('is reply', $body->isReply());
    var_dump('replied Email id', $body->getRepliedToEmail());
    var_dump('message-id', $body->getMessageId());
} else {
    echo json_encode(['Error' => 'Invalid Data']);
}

```